# frozen_string_literal: true

module LicenseFinder
  class Maven
    XML_PARSE_OPTIONS = {
      'ForceArray' => %w[license dependency],
      'GroupTags' => {
        'licenses' => 'license',
        'dependencies' => 'dependency'
      }
    }.freeze

    def active?
      project_path.join('pom.xml').exist?
    end

    def prepare
      within_project_path do
        tool_box.install(tool: :java, version: java_version, env: default_env)
      end
    end

    def current_packages
      within_project_path do
        return [] unless shell.execute(detect_licenses_command, env: default_env)[-1].success?

        resource_files.flat_map { |file| map_from(file.read) }.uniq
      end
    end

    private

    def java_version(env: ENV)
      @java_version ||= tool_box.version_of(:java, env: env)
    end

    def default_env
      @default_env = {
        'CACHE_DIR' => '/opt/gitlab',
        'JAVA_HOME' => ENV.fetch("JAVA_HOME", "/opt/asdf/installs/java/#{java_version}")
      }
    end

    def detect_licenses_command
      [
        package_management_command,
        "-e",
        "org.codehaus.mojo:license-maven-plugin:aggregate-download-licenses",
        "-Dlicense.excludedScopes=#{@ignored_groups.to_a.join(',')}",
        "-Dorg.slf4j.simpleLogger.log.org.codehaus.mojo.license=debug",
        ENV.fetch('MAVEN_CLI_OPTS', '-DskipTests')
      ]
    end

    def resource_files
      Pathname.glob(project_path.join('**', 'target', 'generated-resources', 'licenses.xml'))
    end

    def map_from(xml)
      log.debug(xml)
      XmlSimple
        .xml_in(xml, XML_PARSE_OPTIONS)['dependencies']
        .map { |dependency| Dependency.from(MavenPackage.new(dependency), detected_package_path) }
    end

    def package_management_command
      wrapper? ? project_path.join('mvnw') : :mvn
    end

    def wrapper?
      project_path.join('mvnw').exist?
    end
  end
end
